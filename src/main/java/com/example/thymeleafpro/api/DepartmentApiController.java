package com.example.thymeleafpro.api;

import java.net.http.HttpRequest;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.thymeleafpro.model.Department;
import com.example.thymeleafpro.service.DepartmentService;

@RestController
@RequestMapping("/api/department")
@CrossOrigin
public class DepartmentApiController {
	@Autowired
	DepartmentService departmentService;
	
	@RequestMapping(value="/list", method = RequestMethod.GET)
	public ResponseEntity<?> listDepartment() {
		return new ResponseEntity<>(departmentService.listAll(),HttpStatus.OK);
	}
	@RequestMapping(value="/get/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getDepartment(@PathVariable int id) {
		return ResponseEntity.ok(departmentService.getById(id).orElse(new Department()));
	}
	
	@RequestMapping(value="/add", method = RequestMethod.POST)
	public ResponseEntity<?> addDepartment(@RequestBody Department department)
	{
		return ResponseEntity.ok(departmentService.add(department));
	}
	
	@RequestMapping(value="/edit", method = RequestMethod.PUT)
	public ResponseEntity<?> editDepartment(@RequestBody Department department)
	{
		return ResponseEntity.ok(departmentService.update(department));
	}
	
	@RequestMapping(value="/delete/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteDepartment(@PathVariable int id) {
		departmentService.delete(id);
		return ResponseEntity.ok("");
	}

}
