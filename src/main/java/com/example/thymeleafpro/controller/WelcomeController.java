package com.example.thymeleafpro.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class WelcomeController {
	
	@RequestMapping(value="/welcome",method = RequestMethod.GET)
	public String welcome() {
		return "/welcome";		
	}
	@RequestMapping(value="/",method = RequestMethod.GET)
	public String welcomeAgain() {
		return "/welcome";		
	}
}
