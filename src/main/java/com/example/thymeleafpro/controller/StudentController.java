package com.example.thymeleafpro.controller;

import javax.persistence.Id;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.example.thymeleafpro.model.Department;
import com.example.thymeleafpro.model.Student;
import com.example.thymeleafpro.service.DepartmentService;
import com.example.thymeleafpro.service.StudentService;

import javassist.expr.NewArray;

@Controller
@RequestMapping("/student")
public class StudentController {
	@Autowired
	StudentService studentService;
	@Autowired
	DepartmentService departmentService;
	@RequestMapping(value = "/list",method = RequestMethod.GET)
	public ModelAndView index()
	{
		ModelAndView mv = new ModelAndView("student/index");
		mv.addObject("students", studentService.listAll());
		return mv;
	}
	
	@RequestMapping(value = "/add",method = RequestMethod.GET)
	public ModelAndView add() {
		ModelAndView mv = new ModelAndView("student/create");
		mv.addObject("student",new Student());
		mv.addObject("departments",departmentService.listAll());
		return mv;
	}
	@RequestMapping(value = "/add",method = RequestMethod.POST)
	public ModelAndView addStudent(@ModelAttribute Student student) {
		ModelAndView mv = new ModelAndView("student/index");
		student.setDepartment(departmentService.getById(student.getDepartmentId()).orElse(new Department()));
		studentService.add(student);
		mv.addObject("students",studentService.listAll());
		return mv;
	}
	@RequestMapping(value = "/edit/{id}",method = RequestMethod.GET)
	public ModelAndView edit(@PathVariable int id) {
		ModelAndView mv = new ModelAndView("student/edit");
		Student student=studentService.getById(id);
		mv.addObject("student",student);
		mv.addObject("departments",departmentService.listAll());
		return mv;
	}
	
	@RequestMapping(value = "/edit",method = RequestMethod.POST)
	public ModelAndView editStudent(@ModelAttribute Student student) {
		ModelAndView mv = new ModelAndView("student/index");
		student.setDepartment(departmentService.getById(student.getDepartmentId()).orElse(new Department()));
		studentService.update(student);
		mv.addObject("students",studentService.listAll());
		return mv;
	}
	
	@RequestMapping(value = "/delete/{id}",method = RequestMethod.GET)
	public ModelAndView delete(@PathVariable int id) {
		ModelAndView mv = new ModelAndView("student/index");
		studentService.delete(id);
		mv.addObject("students", studentService.listAll());
		return mv;
	}
	
}
