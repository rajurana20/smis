----------------------------------------------------
Web App Information and Instalation Guide
----------------------------------------------------
1. Requirements 
a. Eclipse to debug and run the spring boot application
b. Xampp server for database with default configuration
	default configuration would be
	username = root, password ="" (meaning no password) and running on port:3306
----------------------------------------------------
2. Actions to perform
a. Download and install eclipse.. 
    and install the plugin STS 4 in eclipse.. Help>>Marketplace>> search STS 4 >> Install
b. Install xampp. Then start apache server and mysql... 
   default port of apache : 80 and mysql : 3306
c. go to web brower like chrome .. and type url localhost:80/phpmyadmin
   since apache server running on port 80
d. Create database "smis"
-----------------------------------------------------
3. run the project... as spring boot app
.....................................................
4. goto chrome or any browser.. and goto url localhost:4040