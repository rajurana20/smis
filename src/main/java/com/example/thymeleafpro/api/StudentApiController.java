package com.example.thymeleafpro.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.thymeleafpro.model.Department;
import com.example.thymeleafpro.model.Student;
import com.example.thymeleafpro.service.DepartmentService;
import com.example.thymeleafpro.service.StudentService;

@RestController
@RequestMapping("/api/student")
@CrossOrigin
public class StudentApiController {
	@Autowired
	StudentService studentService;
	@Autowired
	DepartmentService departmentService;
	
	@RequestMapping(value = "/list",method = RequestMethod.GET)
	public ResponseEntity<?> index()
	{
		return ResponseEntity.ok(studentService.listAll());
	}
	@RequestMapping(value = "/get/{id}",method = RequestMethod.GET)
	public ResponseEntity<?> get(@PathVariable int id)
	{
		return ResponseEntity.ok(studentService.getById(id));
	}
	
	@RequestMapping(value = "/add",method = RequestMethod.POST)
	public ResponseEntity<?> addStudent(@RequestBody Student student) {
		student.setDepartment(departmentService.getById(student.getDepartmentId()).orElse(new Department()));
		return ResponseEntity.ok(studentService.add(student));
	}
	@RequestMapping(value = "/edit",method = RequestMethod.PUT)
	public ResponseEntity<?> editStudent(@RequestBody Student student) {
		student.setDepartment(departmentService.getById(student.getDepartmentId()).orElse(new Department()));
		return ResponseEntity.ok(studentService.update(student));
	}
	
	@RequestMapping(value = "/delete/{id}",method = RequestMethod.DELETE)
	public ResponseEntity<?> delete(@PathVariable int id) {
		studentService.delete(id);
		return ResponseEntity.ok("");
	}

}
