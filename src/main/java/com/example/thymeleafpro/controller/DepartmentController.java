package com.example.thymeleafpro.controller;

import java.lang.ProcessBuilder.Redirect;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.example.thymeleafpro.model.Department;
import com.example.thymeleafpro.service.DepartmentService;

import javassist.expr.NewArray;

@Controller
@RequestMapping("/department")
public class DepartmentController {
	
	@Autowired
	DepartmentService departmentService;
	@RequestMapping("/list")
	public ModelAndView listDepartment() {
		ModelAndView mv= new ModelAndView();
		List<Department> departments= departmentService.listAll();
		mv.addObject("departments",departments);
		mv.setViewName("department/index");
		return mv;
	}
	
	
	@RequestMapping(value="/add", method = RequestMethod.GET)
	public ModelAndView addDepartment() {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("department/create");
		mv.addObject("department", new Department());
		return mv;
	}
	
	@RequestMapping(value="/add", method = RequestMethod.POST)
	public ModelAndView addDepartment(@ModelAttribute Department department)
	{
		ModelAndView mv= new ModelAndView("department/index");
		departmentService.add(department);
		mv.addObject("departments", departmentService.listAll());
		return mv;
	}
	
	@RequestMapping(value="/edit/{id}", method = RequestMethod.GET)
	public ModelAndView editDepartment(@PathVariable int id) {
		ModelAndView mv=new ModelAndView();
		mv.setViewName("department/edit");
		Department department= departmentService.getById(id).orElse(new Department());
		mv.addObject("department",department);
		return mv;
	}
	
	@RequestMapping(value="/edit", method = RequestMethod.POST)
	public ModelAndView editDepartment(@ModelAttribute Department department)
	{
		ModelAndView mv= new ModelAndView("department/index");
		departmentService.update(department);
		mv.addObject("departments",departmentService.listAll());
		return mv;
	}
	
	@RequestMapping(value="/delete/{id}", method = RequestMethod.GET)
	public ModelAndView deleteDepartment(@PathVariable int id) {
		ModelAndView mv=new ModelAndView();
		mv.setViewName("department/index");
		departmentService.delete(id);
		mv.addObject("departments",departmentService.listAll());
		return mv;
	}
}
