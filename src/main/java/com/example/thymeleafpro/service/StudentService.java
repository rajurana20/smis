package com.example.thymeleafpro.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.example.thymeleafpro.model.Student;

@Component
public interface StudentService {

	List<Student> listAll();
	Student getById(int id);
	Student add(Student student);
	Student update(Student student);
	String delete(int id);
}
