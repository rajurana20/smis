package com.example.thymeleafpro.implemention;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Component;

import com.example.thymeleafpro.model.Department;
import com.example.thymeleafpro.repository.DepartmentRepository;
import com.example.thymeleafpro.service.DepartmentService;

@Component
public class DepartmentImplement implements DepartmentService {
	@Autowired
	DepartmentRepository departmentRepository;
	@Override
	public List<Department> listAll() {
		// TODO Auto-generated method stub
		return departmentRepository.findAll();
	}
	@Override
	public Optional<Department> getById(int id) {
		// TODO Auto-generated method stub
		return departmentRepository.findById(id);
	}

	@Override
	public Department add(Department department) {
		// TODO Auto-generated method stub
		return departmentRepository.save(department);
	}

	@Override
	public String delete(int id) {
		// TODO Auto-generated method stub
		departmentRepository.deleteById(id);
		return "Deleted Successful";
	}
	@Override
	public Department update(Department department) {
		// TODO Auto-generated method stub
		return departmentRepository.save(department);
	}

}
