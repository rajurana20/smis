package com.example.thymeleafpro.implemention;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.thymeleafpro.model.Department;
import com.example.thymeleafpro.model.Student;
import com.example.thymeleafpro.repository.DepartmentRepository;
import com.example.thymeleafpro.repository.StudentRepository;
import com.example.thymeleafpro.service.StudentService;

@Component
public class StudentImplement implements StudentService {
	@Autowired
	StudentRepository studentRepository;
	@Autowired
	DepartmentRepository departmentRepository;
	@Override
	public List<Student> listAll() {
		// TODO Auto-generated method stub
		return studentRepository.findAll();
	}

	@Override
	public Student getById(int id) {
		// TODO Auto-generated method stub
		return studentRepository.findById(id).orElse(new Student());
	}

	@Override
	public Student add(Student student) {
		// TODO Auto-generated method stub
		student.setDepartment(departmentRepository.findById(student.getDepartmentId()).orElse(new Department()));
		return studentRepository.save(student);
	}

	@Override
	public Student update(Student student) {
		student.setDepartment(departmentRepository.findById(student.getDepartmentId()).orElse(new Department()));
		return studentRepository.save(student);
	}

	@Override
	public String delete(int id) {
		// TODO Auto-generated method stub
		studentRepository.deleteById(id);
		return "Deleted Successfully";
	}

}
