package com.example.thymeleafpro.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.thymeleafpro.model.Student;

public interface StudentRepository extends JpaRepository<Student, Integer> {

}
