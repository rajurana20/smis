package com.example.thymeleafpro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "com.example.thymeleafpro.repository")
public class ThymeleafProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(ThymeleafProjectApplication.class, args);
	}
}
