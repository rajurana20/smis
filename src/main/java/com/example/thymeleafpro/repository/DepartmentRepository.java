package com.example.thymeleafpro.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

import com.example.thymeleafpro.model.Department;

@Component
public interface DepartmentRepository extends JpaRepository<Department, Integer> {
	
	@Modifying
	@Query("update Department d set d.departmentName = ?1, d.description = ?2 where d.departmentId = ?3")
	void update(String departmentName, String description, Integer departmentId);
}
