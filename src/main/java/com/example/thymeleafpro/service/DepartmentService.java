package com.example.thymeleafpro.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.example.thymeleafpro.model.Department;

@Component
public interface DepartmentService {
	List<Department> listAll();
	Optional<Department> getById(int id);
	Department add(Department department);
	Department update(Department department);
	String delete(int id);
}
